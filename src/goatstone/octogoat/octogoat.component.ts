import { Component, ViewChildren, AfterViewInit, Inject } from '@angular/core'
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs/Observable'
import { Product } from 'goatstone/octogoat/models/product'
import * as productReduce from 'goatstone/octogoat/reducers/product'
import * as productAction from 'goatstone/octogoat/actions/product'
import { OctopartAPIService, OPAPII } from 'goatstone/octogoat/services/octopart-api.service'
import { ReplaySubject } from 'rxjs/ReplaySubject'
import { MdDialog, MdDialogRef } from '@angular/material';
import { MailToComponent } from 'goatstone/octogoat/components/mail-to.component'
import { AboutDialogContent } from 'goatstone/octogoat/components/about-dialog-content.component'

@Component({
  selector: 'goatstone-octogoat',
  template: `
<octogoat-header id="my-page-card"
  (searchEvent)="submitSearch($event)"
  (openDialogEvent)="openDialog($event)"
>
</octogoat-header>

<pagination-controls *ngIf="resultsCount > itemsPerPage"
  (pageChange)="pageChange($event)">
</pagination-controls>

<section>
  <article *ngIf="(products$ | async).length === 0"  class="start-message">
    <p>
      Welcome to Octogoat, a search engine for electronic parts.
    </p>
    <p>
      Type in a search term in the text input field above, 
      press the search button and you should see the results of your search here.
    </p>
  </article>

  <article *ngFor="let product of products$ | async | paginate:
  { itemsPerPage: itemsPerPage, currentPage: pageNumber, totalItems: resultsCount };
  index as i"
  (click)="showDetails(product)"
  class="results"
  >
    <div class="img-container">
      <img src="{{product.image[2]}}" />
      <a href="{{ product.image[1] }}"
       target="new"
      >{{ product.image[0] }}</a>
    </div>
    <h4>{{ product.brand }} </h4>
    <p>{{ product.snippet }}</p>
  </article>
</section>
`,
styleUrls: ['./app.component.scss']
})

export class OctogoatComponent implements AfterViewInit{
  public products$: Observable<Product[]>
  public octopartAPI$: ReplaySubject<OPAPII>
  readonly itemsPerPage = 10
  public pageNumber: number = 1
  public searchPhrase: string
  public resultsCount = 0

  constructor (
      private store: Store<productReduce.State>,
      private octopartAPI: OctopartAPIService,
      private dialog: MdDialog
  ) {
    this.products$ = store.select(productReduce.getProducts)
    this.octopartAPI$ = octopartAPI.getSubject()
    this.octopartAPI$.subscribe( x => {
      this.resultsCount = Math.min(x.data[0][0], 990)
      this.searchPhrase = x.data[0][1]
      this.store.dispatch(
        {
          type: productAction.SET_PRODUCTS,
          payload: octopartAPI.getProductsFromPayload(x)
        }
        )
    })
  }
  public showDetails (e) {
    this.openDialog ('product-detail', {data: e})
  }
  public ngAfterViewInit() {
    // this.submitSearch('led')
  }
  public pageChange (page: number) {
    this.pageNumber = page
    let startIndex = this.pageNumber - 1
    this.octopartAPI.partSearch(this.searchPhrase, startIndex)
  }
  public submitSearch (event: any) {
    this.pageNumber = 1
    this.octopartAPI.partSearch(event, 0)
  }
  public openDialog (which: string, content = {}) {
    let data = content
    let selectedDialogContent
    if (which === 'mail') {
      selectedDialogContent = MailToComponent
    } else if (which === 'info') {
      selectedDialogContent = AboutDialogContent
    } else {
      selectedDialogContent = ProductDetailsDialogContent
    }
    let dialogRef = this.dialog.open(selectedDialogContent, data)
    dialogRef.afterClosed().subscribe(result => {
    })
  }
}
import { MD_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'ocotogoat-product-details',
  template: `
  <md-dialog-content>
    <h4>{{ data.brand }}</h4>
    <p>{{ data.snippet }}</p>
    <article>
      <div class="img-container" *ngFor="let img of data.images;
      index as i">
        <img src="{{img[2]}}" />
        <a href="{{ img[1] }}"
        target="new"
        >{{ img[0] }}</a>
      </div>
    </article>
    <div *ngFor="let s of specs;  index as i">
    {{s}}
    </div>
  </md-dialog-content>
  `,
  styles: [`
h4 {
  font-size: 20px;
  color: #559;
  margin: 0;
}
.img-container {
  display: inline-flex;
  flex-direction: column;
  margin: 0 6px 0 0;
  padding: 2px;
}
a {
   font-size: 10px;
   color: #77f;
   text-decoration: none;
}
article {
  padding: 9px;
  margin: 3px;
}
`
  ]
})
export class ProductDetailsDialogContent { 
  public specs: string[]
  constructor(@Inject(MD_DIALOG_DATA) public data: any) {
    this.specs = Object.keys(data.spec).map( k => {
      return `${data.spec[k].metadata.name} : ${data.spec[k].display_value}`
    })
   }
}
