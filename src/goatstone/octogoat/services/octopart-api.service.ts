import { Injectable } from '@angular/core'
import { Http, Response, RequestOptions, URLSearchParams } from '@angular/http'
import { Observable } from 'rxjs/Observable'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { ReplaySubject } from 'rxjs/ReplaySubject'

export interface OPAPII {
    data: any
}

@Injectable ()
export class OctopartAPIService {
    public oSubject: ReplaySubject<OPAPII>
    constructor (
        private http: Http
    ) {
        this.oSubject = new ReplaySubject()
    }
    public getProductsFromPayload (lineJSON: any) {
        return lineJSON.data[1].map(x => {
            return {
                snippet: x[0],
                brand: x[1][0],
                spec: x[2],
                images: x[3],
                image: (x[3][0] || ['No Image available', '#', ''])
        }})
    }
    public getSubject(): ReplaySubject<OPAPII> {
        return this.oSubject
    }
    public partSearch (q, start) {
        this.getProducts(q, start)
        .subscribe(x => {
            this.oSubject.next(x)
        })
    }
    private getProducts (q: string, start: number) {
        let query = `q=${q}&start=${start}`
        let options = new RequestOptions({
            search: new URLSearchParams(query)
        })

        return this.http.get('http://octogoat.com:5000/part/search', options )
        .map((res: Response) => {
            return res.json()
        })
        .catch(this.handleError)
    }
    private handleError (error: Response | any) {
        let errMsg: string
        if (error instanceof Response) {
            const body = error.json() || ''
            const err = body.error || JSON.stringify(body)
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`
        } else {
            errMsg = error.message ? error.message : error.toString()
        }
        return Observable.throw(errMsg)
    }
}
