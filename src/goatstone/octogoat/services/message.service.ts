import { Injectable } from '@angular/core'
import { Http, Response } from '@angular/http'
import { Headers, RequestOptions } from '@angular/http'
import { Observable } from 'rxjs'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/toPromise'

@Injectable()
export class MessageService {
    constructor(private http: Http) { }
    send(data: any): Observable<any> {
        let url = 'http://goatstone.net:8080/msg'
        let headers = new Headers({ 'Content-Type': 'application/json' })
        let options = new RequestOptions({ headers: headers })
        return this.http.post(url, data, options)
            .catch(this.handleErrorObservable)
    }
    private handleErrorObservable (error: Response | any) {
        console.error(error.message || error)
        return Observable.throw(error.message || error)
    }
    private handleErrorPromise (error: Response | any) {
        console.error('error: ', error.message || error)
        return Promise.reject(error.message || error)
    }
}
