
export interface Product {
    name?: string
    snippet: string
    description?: string
}
