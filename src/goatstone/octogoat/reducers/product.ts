import { Action } from '@ngrx/store'
import { Product } from 'goatstone/octogoat/models/product'
import * as productAction from 'goatstone/octogoat/actions/product'

export interface State {
    products: Product[]
}
const initialState: State = {
    products: []
}
export function reducer(state = initialState, action: Action): State{
    switch (action.type) {
        case productAction.SET_PRODUCTS: {
            return {products: action.payload}
        }
        case productAction.ADD_PRODUCT: {
            return {products: [...state.products, action.payload]}
        }
        case productAction.REMOVE_PRODUCT: {
            return {products: state.products.filter((x, i) => action.payload !== i)}
        }
        default: {
            return state
        }
    }
}
export const getProducts = (state: State) => state.products
