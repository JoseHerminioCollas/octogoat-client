import { Component } from '@angular/core'

@Component({
  selector: 'dialog-overview-example-dialog',
  template: `
<h3>
Octogoat 1.0
</h3>
<p>
Octogoat is a search engine for electronics parts. It uses the 
Octopart API from <a href="https://octopart.com/" target="new">octopart.com</a>.
</p>
<p>
A user can type in a search term, press the search button and see the results the search term brings in. The results of the search are shown to the user in sets. If there are 500 results from the query only a portion of these results are shown. A paginator is displayed that enables the user to navigate the results a set at a time. An image, brand name and a snippet of information is displayed for each item of the result set. The user can click on the result item to open a dialog window that will display more information about the product. This information will consist of more photographs, a snippet of information, the brand and specifications of the product. The user should be able to easily search for any product with a keyword search and navigation of the result set. In the future, search by category or other characteristics of the products will be offered.
</p>
<p>
 Octopart has been designed and developed by 
 <a href="http://goatstone.com/">Goatstone</a>. 
 The development is done with Angular 2 and Material design components. 
 It uses a responsive design that can be used on the desktop or a mobile phone. 
</p>
<p>
Please read carefully the Terms of Service for this site before you continue.
The TOC can be found here <a href="toc.html">toc.html</a>
</p>

<p>
(C) 2017 Goatstone
</p>
`,
styles: [`
h3 {
  margin: 3px 0;
}
p {
  margin: 6px 0;
}`
]
})
export class AboutDialogContent { }
