import { Component, Input, Output, ViewChildren, AfterViewInit, EventEmitter } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'

@Component({
  selector: 'octogoat-search-control',
  template: `
<form [formGroup]="searchForm" (ngSubmit)="submitSearch($event)" novalidate>
  <button type="submit" [disabled]="searchForm.pristine" md-icon-button>
    <md-icon>search</md-icon>
  </button>
  <input mdInput value="LED"
    size=16
    formControlName="query"
    name="x" autofocus #queryField>
</form>
`,
styles: [
  `
  form {
    display: flex;
    align-items: center;
  }
  button {
    border: 1px solid hsla(50, 50%, 100%, 0.7);
    margin-left: 6px;
  }
  input {
    background: #eee;
    color: #115;
    padding: 0 0 3px 3px;
    border-radius: 3px;
    margin-left: 10px;
  }`
]
})

export class SearchControlComponent implements AfterViewInit{
  public searchForm: FormGroup
  @Output() searchEvent: EventEmitter<string> = new EventEmitter<string>()
  @ViewChildren('queryField') qf

  constructor (
      private fb: FormBuilder
  ) {
    this.searchForm = fb.group({
      query: ['', Validators.required]
    })
  }
  public ngAfterViewInit() {
    this.qf.first.nativeElement.focus()
  }
  public submitSearch (event: any) {
    const q = this.searchForm.value.query
    this.searchEvent.emit(q)
  }
}
