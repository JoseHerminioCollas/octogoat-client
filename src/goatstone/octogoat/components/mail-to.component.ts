import { Component, Input, Output, EventEmitter } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MdDialog, MdDialogRef } from '@angular/material';
import { MessageService } from 'goatstone/octogoat/services/message.service'

@Component({
  selector: 'ocotogoat-mailto',
  template: `
<form 
  [formGroup]="todoForm" 
  (ngSubmit)="sendData(todoForm.value)" 
  >
  <md-card>
      <md-card-title>Send Email to Octogoat</md-card-title>
      <md-card-content>
        <md-input-container md-error>
          <label>
            <span>*</span> Name
            <input formControlName="name" mdInput autofocus maxlength="50">
          </label>
        </md-input-container>
        <md-input-container md-error>
          <label>
            <span>*</span> Email
            <input formControlName="email" mdInput autofocus maxlength="50">
          </label>
        </md-input-container>
        <md-input-container md-error>
          <label>
            <span>*</span> Subject
            <input formControlName="subject" mdInput autofocus maxlength="50">
          </label>
        </md-input-container>
        <md-input-container>
          <label>Message
            <textarea formControlName="message" mdInput maxlength="200">
            </textarea>
          </label>
        </md-input-container>

        <button [disabled]="!todoForm.valid" type="submit" md-fab>
          <md-icon>send</md-icon>
        </button>

      </md-card-content>
  </md-card>
</form>
`,
  styleUrls: [ './mail-to.css' ]
})

export class MailToComponent {
  // @Output() emitDataEvent: EventEmitter<any> = new EventEmitter()
  public todoForm = this.fb.group({
    name: ['', Validators.required],
    email: ['', Validators.required],
    subject: ['', Validators.required],
    message: ['']
  })

  constructor (
    private fb: FormBuilder,
    public dialogRef: MdDialogRef<MailToComponent>,
    private messageService: MessageService
    ) {
  }
  private msg (data: any){
    let o = this.messageService.send(data)
    o.subscribe(x => {
    })
  }
  private sendData (data: any) {
      if (data) {
          this.msg(data)
      }
      // this.emitDataEvent.emit(data)
      this.dialogRef.close()
  }
}
