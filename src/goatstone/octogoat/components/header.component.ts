import { Component, Input, Output, EventEmitter, 
    ViewChildren, AfterViewInit } from '@angular/core'
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs/Observable'
import { Product } from 'goatstone/octogoat/models/product'
import * as productReduce from 'goatstone/octogoat/reducers/product'
import * as productAction from 'goatstone/octogoat/actions/product'
import { OctopartAPIService, OPAPII } from 'goatstone/octogoat/services/octopart-api.service'
import { ReplaySubject } from 'rxjs/ReplaySubject'

@Component({
  selector: 'octogoat-header',
  template: `
<md-toolbar color="primary">

  <div class="header-left">
    <h1>Octogoat</h1>
    <octogoat-search-control
      (searchEvent)="submitSearch($event)"
    ></octogoat-search-control>
  </div>

  <div class="header-right">
    <button md-icon-button
      (click)="openDialog('mail')">
      <md-icon md-large>mail</md-icon>
    </button>
    <button md-icon-button
      (click)="openDialog('info')">
      <md-icon md-large>info</md-icon>
    </button>
  </div>

</md-toolbar>

`,
styles: [`
h1 {
  font-size: 1.3em;
  margin-bottom: 30px;
}
/deep/ .mat-toolbar-row {
  justify-content: space-between;
}
/deep/ .mat-toolbar-row .header-left {
  display: flex;
  justify-content: space-between;
  align-items: center;
}
`]
})

export class HeaderComponent {
  @Output() searchEvent: EventEmitter<string> = new EventEmitter<string>()
  @Output() openDialogEvent: EventEmitter<string> = new EventEmitter<string>()

  constructor () { }
  public submitSearch (event: any) {
    this.searchEvent.emit(event)
  }
  public openDialog (e: any) {
    this.openDialogEvent.emit(e)
  }
}
