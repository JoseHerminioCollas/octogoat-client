import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { StoreModule } from '@ngrx/store'
import { MdListModule, MdToolbarModule, MdIconModule, 
  MdButtonModule, MdInputModule, MdDialogModule } from '@angular/material'
import { NgxPaginationModule } from 'ngx-pagination'
import { EffectsModule } from '@ngrx/effects'
import { OctogoatComponent } from 'goatstone/octogoat/octogoat.component'
import { reducer } from 'goatstone/octogoat/reducers/product'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { OctopartAPIService } from 'goatstone/octogoat/services/octopart-api.service'
import { SearchControlComponent } from 'goatstone/octogoat/components/search-control.component'
import { HeaderComponent } from 'goatstone/octogoat/components/header.component'
import { AboutDialogContent } from 'goatstone/octogoat/components/about-dialog-content.component'
import { MailToComponent } from 'goatstone/octogoat/components/mail-to.component'
import { MessageService } from 'goatstone/octogoat/services/message.service'
import { ProductDetailsDialogContent } from 'goatstone/octogoat/octogoat.component'

@NgModule({
  imports:      [
    BrowserModule,
    BrowserAnimationsModule,
    NgxPaginationModule,
    StoreModule.provideStore(reducer),
    HttpModule,
    ReactiveFormsModule,
    MdListModule,
    MdToolbarModule,
    MdIconModule,
    MdButtonModule,
    MdInputModule,
    MdDialogModule
    ],
  declarations: [
    OctogoatComponent,
    SearchControlComponent,
    HeaderComponent,
    AboutDialogContent,
    MailToComponent,
    ProductDetailsDialogContent
    ],
  providers: [
    OctopartAPIService,
    MessageService
    ],
  entryComponents: [
    AboutDialogContent,
    MailToComponent,
    ProductDetailsDialogContent
    ],
  bootstrap: [
    OctogoatComponent
    ]
})
export class OcotogoatModule { }
