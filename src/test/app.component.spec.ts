/* tslint:disable: max-line-length */
import { TestBed, async } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { ReactiveFormsModule } from '@angular/forms'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { MaterialModule } from '@angular/material'
import { MdListModule, MdToolbarModule, MdIconModule,
  MdButtonModule, MdInputModule, MdDialogModule } from '@angular/material'
import { HttpModule } from '@angular/http'
import { By }              from '@angular/platform-browser'

import { NgxPaginationModule } from 'ngx-pagination'
import { SearchControlComponent } from 'goatstone/octogoat/components/search-control.component'

import { OctogoatComponent } from 'goatstone/octogoat/octogoat.component';
import { HeaderComponent } from 'goatstone/octogoat/components/header.component'
import { AboutDialogContent } from 'goatstone/octogoat/components/about-dialog-content.component'
import { MailToComponent } from 'goatstone/octogoat/components/mail-to.component'
import { MessageService } from 'goatstone/octogoat/services/message.service'
import { ProductDetailsDialogContent } from 'goatstone/octogoat/octogoat.component'
import { OctopartAPIService } from 'goatstone/octogoat/services/octopart-api.service'

import { StoreModule } from '@ngrx/store'
import { reducer } from 'goatstone/octogoat/reducers/product'

import 'rxjs/add/operator/takeUntil'

describe('Octogoat Main Component', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        HttpModule,
        MaterialModule,
        MdListModule,
        MdToolbarModule,
        MdIconModule,
        MdButtonModule,
        MdInputModule,
        MdDialogModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        StoreModule.provideStore(reducer)
        ],
      providers: [OctopartAPIService],
      declarations: [OctogoatComponent, HeaderComponent, SearchControlComponent]
    })
  })

  it('should display a header element', () => {
    const fixture = TestBed.createComponent(OctogoatComponent)
    fixture.detectChanges()
    let headers = fixture.debugElement.queryAll(By.directive(HeaderComponent))
    expect(headers.length).toBeGreaterThan(0)
  })

})
