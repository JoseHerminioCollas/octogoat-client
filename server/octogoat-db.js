let MongoClient = require('mongodb').MongoClient;

class OctogoatDB {
    constructor() { }
    connect () {
        return new Promise((resolve, reject) => {
            MongoClient.connect("mongodb://localhost:27017/octogoat")
                .then(db => {
                    this.db = db
                    resolve()
                })
        })
    }
    addClientRequest(request) {
        this.db.collection('clientRequest', {}, function(err, collection) {
            collection.insert(request)
        });
    }
    close() {
        if (this.db) {
            this.db.close()
            .then(
                function() {},
                function(error) {
                    console.log("Failed to close the database: " + error.message)
                }
            )	
        }
    }
}
module.exports = OctogoatDB
