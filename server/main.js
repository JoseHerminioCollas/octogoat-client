let Rx = require('rx')

let one = require("./one")
let OctogoatDB = require('./octogoat-db')

// create a stream for server events
const serverEvent$ = new Rx.BehaviorSubject({message: 'start', timestamp: new Date})
const db = new OctogoatDB()
// connect the DB
db.connect()
.then(c => {
    // subscribe to the stream
    serverEvent$.subscribe(x => {
        db.addClientRequest(x)
    })
    // start the server with the stream
    one(serverEvent$)
})
