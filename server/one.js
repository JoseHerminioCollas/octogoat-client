"use strict"
const compression = require('compression')
const express = require('express')
const router = express.Router();
const path = require('path')

const PROD_PORT = 8088
const PORT = PROD_PORT
const ip = require('ip')
const HOST = ip.address()
const app = express()
app.use(compression())
app.use(express.static('../dist/client/assets/'))

function one (bs) {
  router.use(function(req, res, next) {
    next()
  })
  router.get('/', function(req, res, next) {
      bs.onNext({message: 'GET', timestamp: new Date()})
      res.sendFile(path.resolve(__dirname, '../dist/client/index.html'))
  })
  app.use('/', router)
  app.listen(PORT, () => {
    console.log(`:Listening on: http://${HOST}:${PORT}`)
  })
}
module.exports = one