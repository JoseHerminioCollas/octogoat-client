# Octogoat Web Client

## Based on Starter Seed Project: Material Branch without Universal (Server-side rendering) support

> Featuring Material Design 2, Webpack 2(and Webpack DLL plugin for faster dev builds), HMR (Hot Module Replacement), and @ngrx for state management.

```bash
https://github.com/qdouble/angular-webpack2-starter.git
```
